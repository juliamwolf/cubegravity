using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainChecker : MonoBehaviour
{

    public bool touchingTerrain = false;

    private void OnTriggerEnter(Collider other)
    {
        touchingTerrain = true;
    }

    private void OnTriggerStay(Collider other)
    {
        touchingTerrain = true;
    }

    private void OnTriggerExit(Collider other)
    {
        touchingTerrain = false;
    }

}
