using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour
{
    public float speed = 5f;
    public float gravity = -9.8f;
    //public float jumpForce = 10f;
    //public float jumpHeight = 2f; //設定跳躍高度 (2f = 2m)
    public float jumpHeight = 2f;

    public Transform groundCheckerTransform;
    public LayerMask groundLayer;
    public float groundDistance = 0.2f;

    private CharacterController controller;

    private Vector3 velocity = Vector3.zero;

    private bool isGrounded = false;
    public TerrainChecker terrainChecker;

    private GameObject myCamera;

    public int chickNumber = 0;
    public int maxChickNumber;

    private HUDManager hudManager;

    private Animator animController;

    private AudioSource cheerSound;

    public AudioClip yaySound;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        myCamera = GameObject.FindWithTag("MainCamera");

        maxChickNumber = GameObject.FindObjectsOfType<Chick>().Length;

        hudManager = GameObject.FindObjectOfType<HUDManager>();

        animController = GetComponent<Animator>();

        cheerSound = GetComponent<AudioSource>();
    }

    
  
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(horizontalInput, 0f, verticalInput);

        //讓追蹤相機的方向正確
        move = Quaternion.Euler(0f, myCamera.transform.eulerAngles.y, 0f) * move;

        controller.Move(move * Time.deltaTime * speed);

        //讓角色身體永遠朝著移動方向
        if (move != Vector3.zero)
        {
            transform.forward = move;
        }

 
        //GroundedCHeck

        isGrounded = Physics.CheckSphere(groundCheckerTransform.position, groundDistance, groundLayer, QueryTriggerInteraction.Ignore);

        if (isGrounded && velocity.y < 0f)
        {
            velocity.y = 0f;
        }
        else
        {
            velocity.y = velocity.y + gravity * Time.deltaTime;
        }

        //Jump

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Debug.Log("Jump");
            velocity.y = velocity.y + Mathf.Sqrt(gravity * jumpHeight * -2f);//Mathf.Sqrt 意思是開根號    
        }

        controller.Move(velocity * Time.deltaTime);

        //UPdate animatorController

        animController.SetFloat("Speed", move.magnitude);
        animController.SetBool("InAir", !isGrounded && !terrainChecker.touchingTerrain);
      
    }

    public void CollectChick()
    {
        chickNumber = chickNumber + 1;
        Debug.Log("Chick #" + chickNumber + " eingelsamelt!");

        if (chickNumber >= maxChickNumber)
        {
            hudManager.ShowWinpanel();
            //Debug.Log("Gewonnen!");

            cheerSound.Play();

            cheerSound.PlayOneShot(yaySound);

        }
    }

}
