using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chick : MonoBehaviour
{

    private float animationOffset;

    private Animator animController;

    public GameObject speialEffect;


    void Awake()
    {
        animationOffset = Random.Range(0f, 1f);

        animController = GetComponent<Animator>();

        animController.SetFloat("Offset", animationOffset);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Fox>().CollectChick();

            Instantiate(speialEffect, transform.position, transform.rotation);

            Destroy(gameObject);
        }
        
    }


}
