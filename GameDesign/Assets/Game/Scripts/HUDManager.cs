using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class HUDManager : MonoBehaviour
{
    public TextMeshProUGUI chickText;
    public Fox player;
    public GameObject winPanel;

    public EventSystem eventSystem;
    public GameObject mainMenuButton;

    // Start is called before the first frame update
    void Start()
    {
         winPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        chickText.text = player.chickNumber + "/" + player.maxChickNumber; 
    }

    public void ShowWinpanel()
    {
        winPanel.SetActive(true);
        eventSystem.SetSelectedGameObject(mainMenuButton);
    }
}
