using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MenuManager : MonoBehaviour
{
    // Start is called before the first frame update


    public AudioMixer mixerGroup;

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void QuitGame()
    {
        Application.Quit();

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#endif

    }

    public void SetMaster(float sliderValue)
    {
        mixerGroup.SetFloat("Volume", Mathf.Log10(sliderValue) * 20);
    }


}

