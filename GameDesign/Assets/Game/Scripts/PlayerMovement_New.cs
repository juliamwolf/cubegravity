using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement_New : MonoBehaviour
{
    [SerializeField]
    private float walkingSpeed;

    [SerializeField]
    private float jumpForce = 5f;

    private Rigidbody rigid;
    private Vector3 playerUpDirection = Vector3.up;
    private Vector3 playerForwardDirection = Vector3.forward;

    private RaycastHit currentRaycastHit;

    [SerializeField]
    private Transform currentCenterOfGravity;


    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Raycast from the character to the intended center of gravity to detect the surface normal:
        Physics.Raycast(transform.position, currentCenterOfGravity.position - transform.position, out currentRaycastHit, 100f);
        //The center of gravity doesn't have to be the cube, anything in the correct position works (eg an empty GameObject). It could also change at any time if you want to be able to jump between objects. Maybe setting it to a new object with a certain tag when you collide with it?


        Physics.gravity = currentRaycastHit.normal * -9.81f;

        Quaternion orientationDirection = Quaternion.FromToRotation(-transform.up, Physics.gravity.normalized) * transform.rotation; //Explanation: https://www.youtube.com/watch?v=v4gkheo0dt8
        transform.rotation = orientationDirection;


        //Movement copied from the old Script:

        Vector3 velocity;
        velocity = transform.forward * Input.GetAxis("Vertical") * walkingSpeed + transform.right * Input.GetAxis("Horizontal") * walkingSpeed;

        transform.position += velocity * Time.deltaTime;

        if (Input.GetButtonDown("Jump")) rigid.AddForce(transform.up * jumpForce, ForceMode.Impulse);

    }
}
