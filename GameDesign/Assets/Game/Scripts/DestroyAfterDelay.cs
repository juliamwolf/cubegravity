using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterDelay : MonoBehaviour
{

    public float delay = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("SelfDestruct", delay);
    }

    // Update is called once per frame
    private void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
