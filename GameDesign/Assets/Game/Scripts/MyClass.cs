using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Document;

public class MyClass : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Document.Table.SaySomething();
        Table.SaySomething();
    }

    // Update is called once per frame
    void Update()
    {

    }

}

namespace Furniture
{
    public class Table : MonoBehaviour
    {
        public static void SaySomething()
        {
            Debug.Log("Ich bin ein Tisch!");
        }
    }
}

namespace Document
{
    public class Table : MonoBehaviour
    {
        public static void SaySomething()
        {
            Debug.Log("Ich bin eine Tabelle!");
        }
    }
}

    

